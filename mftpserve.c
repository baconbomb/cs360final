/* Spencer Bacon
 * cs360
 * 4/20/18
 * mftp server
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include "mftp.h"

void getTime(int connectfd, struct sockaddr_in* clientAddr);

int main(int argc, char** argv){

    int listenfd;
    int connectfd;

    struct sockaddr_in clientAddr;


    listenfd = initSocket();
    bindSocket(49999, listenfd);
    listenSocket(listenfd, 4);


    while(1){       //wait for additional connections
        connectfd = connectSocket(listenfd, &clientAddr);
        int pid = fork();
        if(pid < 0){
            perror("fork");
            exit(EXIT_FAILURE);
        }

        if(pid == 0){               //is child
            close(listenfd);    
            startServer(connectfd, &clientAddr);    //start new server
            exit(EXIT_SUCCESS);
        }
        else {                      //is parent 
            close(connectfd);
        }
    }
}



