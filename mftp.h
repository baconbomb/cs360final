/* Spencer Bacon
 * cs360
 * 4/2/18
 * ftp header file
*/

#define MY_PORT_NUMBER 49999
#define MAXLINE 1024

int initSocket(); 
/*initialize the socket 
 * return socket file descriptor
 */

void clientConnect(int, int, struct sockaddr_in*, struct hostent*);
/* connect a client to a server
 * input
 *      1. port number
 *      2. socket file descriptor
 *      3. serv addr structure pointer
 * no retrun
 */

int bindSocket(int, int); 
/*bind socket
 * input 
 *      1. port number
 *      2. socket file descriptor 
 *
 * returns port number socket is bound to
 */

void listenSocket(int, int);
/*set up socket for listening 
 *
 * input 
 *      1. socket file descriptor
 *      2. number of simultanious connect requenst
 *
 * no return
 */

int connectSocket(int, struct sockaddr_in*);
/* accepts connection on the socket
 *
 * input
 *      1. socket file descriptor
 *      2. client address structure pointer
 *
 * returns connected socket file descriptor
 */ 

void startClient(int, struct hostent*);
/* Start client processes */

int response(int);
/* read response from server */

void rChangeD(char*, int);
/*execute rcd command
 * input
 *      1. pathname string
 *      2. connct socket fd
 * no return
 */
void ls();
/* pipe ls -l -a into more */

void rls(int, struct hostent*);
/* remost ls command piped to more 
 * input
 *      1. socket file descriptor
 *      2. server address structure pointer
 * no return
 */

int openData(int, struct hostent*);
/* create data connection to server 
 * input
 *      1. socket file descriptor
 *      2. server address structure pointer
 * no return
 */

void get(int, char*, struct hostent*);
/* remote get <pathname> 
 * input
 *      1. socket file descriptor
 *      2. path name
 *      3. server address structure pointer
*/

void put(int, char*, struct hostent*);
/* remote put <pathname> 
 * input
 *      1. socket file descriptor
 *      2. path name
 *      3. server address structure point
 * no return
 */

void show(int, char*, struct hostent*);
/*remote show <pathname> 
 * input
 *      1. socket file descriptor
 *      2. path name
 *      3. server address structure point
 * no return
 */

void commandError();
/* command error handler */

void startServer(int, struct sockaddr_in*);
/* Start server process
 * input    
 *      1. socket file descriptor
 *      2. client addr structure pointer
 *  no return
 */  

void sendError(int);
/* send error message to client
 * input
 *      1. socket file desriptor
 * no return
 */

void acknowledge(int);
/* acknowledge command
 * input
 *      1. socket file descriptor
 * return
 */

void getCommand(int, char*);
/* Get command from client
 * input
 *      1. socket file descriptor
 *      2. string pointer
 * no return
 */

int serverData(int);
/* create data socket for server */

void serverLS(int, int);
/* ls for server
 * input
 *      1. socket file descrior
 *      2. socket file descriptor data connection
 * no return
 */

void serverGet(int, int, char*);
/* get file from server
 * input
 *      1. socket file descripto
 *      2. socket file descripto data connectrion
 *      3. pathname
 * no return
 */

void serverPut(int, int, char*);
/* put file onto server
 * input
 *      1. socket file descriptor
 *      2. socket file descriptor data connection
 *      3. pathname
 * no return
 */
