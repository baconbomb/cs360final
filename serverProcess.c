#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>

#include "mftp.h"

void startServer(int connectfd, struct sockaddr_in* clientAddr){
    
    char* hostName;
    char* ip;
    char* command = malloc(sizeof(char) * MAXLINE); 
    int datafd;
    struct hostent* hostEntry;

    hostEntry = gethostbyaddr(&(clientAddr->sin_addr), sizeof(struct in_addr), AF_INET);
    hostName = hostEntry->h_name;
    ip = inet_ntoa(*((struct in_addr*) hostEntry->h_addr_list[0]));
    fprintf(stdout, "New client connected: %s\n", hostName);
    fprintf(stdout, "Client IP address -> %s\n", ip);
    //TODO: get host ip address
    
    while(1){
        getCommand(connectfd, command);     //get command until \n
        
        if(command[0] == 'Q'){
            acknowledge(connectfd);         //send client A\n
            printf("%s has disconnected\n", hostName);
            return;
        }
        else if(command[0] == 'C'){
            if(chdir(command + 1) != 0){    //pathname starts after the C
                sendError(connectfd);       //C<pathname>
            }
            else {
                acknowledge(connectfd);
                printf("Changed current directory to %s\n", command + 1);
            }
        }
        else if(command[0] == 'D'){
            datafd = serverData(connectfd); //open datafd
        }
        else if(command[0] == 'L'){
            serverLS(connectfd, datafd);
        }
        else if(command[0] == 'G'){
            serverGet(connectfd, datafd, command);
        }
        else if(command[0] == 'P'){
            serverPut(connectfd, datafd, command);
        }
        else{
            write(connectfd, "EInvalid client command\n", MAXLINE); //send client invalid error, invalid command
        }
    }
}

void serverPut(int connectfd, int datafd, char* command){
    
    char path[MAXLINE];
    strcpy(path, command + 1);
    char* prev;
    char* curr;

    curr = strtok(path, "/");
    prev = curr;

    while((curr = strtok(NULL, "/")) != NULL){
        prev = curr; //prev is last part of pathname
    }
    
    prev[strlen(prev)] = '\0'; //null terminate to use with open
    
    char buffer[MAXLINE];
    int i;

    int fd = open(prev, O_WRONLY | O_CREAT, 0644);
    
    if(fd == -1){
        sendError(connectfd);           //cannont open file, report error
        close(datafd);
    }

    acknowledge(connectfd);     //open is successful

    printf("Receiving file %s from client\n", prev);

    while((i = read(datafd, buffer, MAXLINE)) > 0){ //read from datafd, write to file
        write(fd, buffer, i);
    }
    close(fd);      //dont forget to close

    close(datafd);
}

void serverGet(int connectfd, int datafd, char* command){

    char buffer[MAXLINE];
    int i;
    command[strlen(command)] = '\0';        //null terminate to use with open

    if(access(command + 1, R_OK) == -1){
        char err[MAXLINE] = "ECannot access file ";
        strcat(err, command + 1);
        strcat(err, "\n");
        write(connectfd, err, MAXLINE);
        return;
    }

    int fd = open(command + 1, O_RDONLY);   //open file
    if(fd == -1){
        sendError(connectfd);               //cannot open file, report error
        close(datafd);
        return;
    }

    printf("Reading file %s\n", command + 1);

    acknowledge(connectfd);         //open successful

    printf("Transmitting file %s to client\n", command + 1);

    while((i = read(fd, buffer, MAXLINE)) > 0){ //read from file, write to datafd
        write(datafd, buffer, i);
    }
    close(fd);
    close(datafd);
}

void serverLS(int connectfd, int datafd){

    acknowledge(connectfd);
    if(!fork()){    //is child
        dup2(datafd, 1);    //pipe stdout to datafd

        if(execlp("ls", "ls", "-l", "-a", (char*) NULL) == -1){ //exec ls -la
            sendError(connectfd);       //failed
        }
    }
    else {
        wait(NULL);     //wait for child
    }
    close(datafd);
}

void sendError(int connectfd){
    char eString[MAXLINE] = "E";
    strcat(eString, strerror(errno));
    strcat(eString, "\n");      //build error command
    write(connectfd, eString, strlen(eString));
}

void getCommand(int connectfd, char* command){

    char input; 
    int i = 0;

    read(connectfd, &input, 1); //read on char at a time

    while(input != '\n'){       //read until \n
        command[i++] = input;
        read(connectfd, &input, 1);
    }

    command[i] = '\0';          //end string with null terminator
}

void acknowledge(int connectfd){
    write(connectfd, "A\n", 2);     //success
}

int serverData(int connectfd){

    int listendatafd, dataPort, datafd;
    struct sockaddr_in dataAddr;
    listendatafd = initSocket();

    dataPort = bindSocket(0, listendatafd);     //get random portnum
    char aData[MAXLINE] = "A";
    char portString[MAXLINE];
    sprintf(portString, "%d", dataPort);
    strcat(aData, portString);
    strcat(aData, "\n");            //build success string
    write(connectfd, &aData, strlen(aData));

    listenSocket(listendatafd, 1);

    datafd = connectSocket(listendatafd, &dataAddr);    //connect to datafd

    return datafd;
}
