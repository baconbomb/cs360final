#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <time.h>

#include "mftp.h"

int initSocket(){
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if(sock < 0){
        perror("socket() failed");
        exit(EXIT_FAILURE);
    }
    return sock;
}

void clientConnect(int portNum, int socketfd, struct sockaddr_in* addr, struct hostent* hostEntry){
    struct in_addr **pptr;

    pptr = (struct in_addr **) hostEntry->h_addr_list;
    memcpy(&(addr->sin_addr), *pptr, sizeof(struct in_addr));

    memset(addr, 0, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_port = htons(portNum);
    
    if(connect(socketfd, (struct sockaddr *) addr, sizeof(struct sockaddr_in))){
        perror("connection: ");
        exit(EXIT_FAILURE);
    }
}

int bindSocket(int portNum, int listenfd){

    struct sockaddr_in servAddr;
    struct sockaddr_in servSock;

    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(portNum);
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(listenfd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {   //bind socket
        perror("bind() failed");
        exit(EXIT_FAILURE);
    }
    memset(&servSock, 0, sizeof(servSock));
    socklen_t length = sizeof(struct sockaddr_in);
    if((getsockname(listenfd, (struct sockaddr *) &servSock, &length)) == -1){
        perror("getsockname() failed");
        exit(EXIT_FAILURE);
    }
    int socketNum = (int) ntohs(servSock.sin_port);
    return socketNum;
}

void listenSocket(int listenfd, int connectNum){

    listen(listenfd, connectNum);    //listen
}

int connectSocket(int listenfd, struct sockaddr_in* clientAddr){
    
    socklen_t length = sizeof(struct sockaddr_in);

    int connectfd = accept(listenfd, (struct sockaddr *) clientAddr, &length); //accept

    if(connectfd < 0){
        perror("accept() failed");
        exit(EXIT_FAILURE);
    }

    return connectfd;
}
