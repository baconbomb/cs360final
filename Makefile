CFLAGS=-g -std=c99 -Wall -pedantic

all: mftp mftpserve

mftp: mftp.c connect.c clientProcess.c
	gcc $(CFLAGS) $^ -o $@

mftpserve: mftpserve.c connect.c serverProcess.c
	gcc $(CFLAGS) $^ -o $@


clean:
	rm -f mftp mftpserve
