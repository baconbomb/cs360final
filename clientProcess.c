#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "mftp.h"

void startClient(int connectfd, struct hostent* hostEntry){

    char input[MAXLINE];
    char* command;
    char* pathname;
    char delim[] = " \t\r\n\v\f";

    printf("MFTP> ");

    while(fgets(input, MAXLINE, stdin)){
        command = strtok(input, delim);
        pathname = NULL;

        if(command == NULL){
            printf("MFTP> ");
            continue;
        }

        if(strcmp(command, "exit") == 0){           //exit command
            write(connectfd, "Q\n", 2);
            if(response(connectfd)){
                break;
            }
        }
        else if(strcmp(command, "rcd") == 0){       //change remote directory command
            pathname = strtok(NULL, delim);         //get path name
            if(pathname == NULL){
                commandError();
                continue;
            }
            rChangeD(pathname, connectfd);
        }
        else if(strcmp(command, "cd") == 0){        //change local directory
            pathname = strtok(NULL, delim);
            if(chdir(pathname) != 0){
                perror("Change directory");
            }
        }
        else if(strcmp(command, "ls") == 0){        //local ls command
            ls();
        }
        else if(strcmp(command, "rls") == 0){       //remote ls command
            rls(connectfd, hostEntry);
        }
        else if(strcmp(command, "get") == 0){       //remoste get file
            pathname = strtok(NULL, delim);
            if(pathname == NULL){
                commandError();
                continue;
            }
            get(connectfd, pathname, hostEntry); 
        }
        else if(strcmp(command, "put") == 0){       //remote put
            pathname = strtok(NULL, delim);
            if(pathname == NULL){
                commandError();
                continue;
            }
            put(connectfd, pathname, hostEntry);
        }
        else if(strcmp(command, "show") == 0){      //remost show
            pathname = strtok(NULL, delim);
            if(pathname == NULL){
                commandError();
                continue;
            }
            show(connectfd, pathname, hostEntry);
        }
        else{
            printf("Command '%s' is unknown - ignored\n", command);
        }

        printf("MFTP> ");
    }
}
    
void commandError(){        //invalid command
    printf("Command error: expection parameter\n");
    printf("MFTP> ");
}

int openData(int connectfd, struct hostent* hostEntry){

    int portNum, datafd;
    struct sockaddr_in dataAddr;
    char response;
    char pNumber[MAXLINE];

    write(connectfd, "D\n", 2); //request data connectrion

    read(connectfd, &response, 1);      //check if accepted
    if(response == 'A'){                //is accepted
        read(connectfd, pNumber, MAXLINE);  //read portnumber
        sscanf(pNumber, "%d", &portNum);    //convery string to int
    }
    else {
        int i = read(connectfd, pNumber, MAXLINE);  //read error from server
        
        pNumber[i] = '\0';              //append null terminator to print error
        fprintf(stdout, "Error response from server: %s", pNumber); 
        return 0;               //return 0 for no data port connectrion
    } 
    datafd = initSocket();

    clientConnect(portNum, datafd, &dataAddr, hostEntry);
    return datafd;
}

int response(int connectfd){

    char response;
    char e[MAXLINE]; 

    read(connectfd, &response, 1);      //check first char response
    int i = read(connectfd, e, MAXLINE);    //read everything after Astring length

    if(response == 'E'){
        if(e[0] != '\n'){           //chech if there is an error message after first char sent
            e[i] = '\0';            //terminate string to print
            fprintf(stdout, "Error response from server: %s", e);   //print error message 
            return 0;           //response filed
        }
    }
    else {
        if(response == 'A'){    //first char is A
            return 1;           //sucess
        }
        else {
            printf("Invalid server response\n");    //not E or A
            return 0;               //failed
        }
    }
    return 0;
}

void rChangeD(char* pathname, int connectfd){

    char str[MAXLINE] = "C";        
    strcat(str, pathname);
    strcat(str, "\n");      //build string "C<pathname>
    write(connectfd, str, strlen(str));
    response(connectfd);    //read response
}

void ls(){

    if(!fork()){        //child

        int fd[2];

        if(pipe(fd) == -1){
            perror("pipe failed");
        }

        if(!fork()){    //child
            close(fd[0]);
            dup2(fd[1], 1);
            close(fd[1]);
            if(execlp("ls", "ls", "-l", "-a", (char*) NULL) == -1){ //exec and pipe ls into more
                perror("ls failed");
            }
        }
        else{           //parent
            close(fd[1]);
            dup2(fd[0], 0);
            close(fd[0]);
            if(execlp("more", "more", "-20", NULL) == -1){  //exec and pipe ls into more
                perror("more failed");
            }
        }
    }        
    else{           //parent
        wait(NULL);     //wait for child to finish
    }
}

void rls(int connectfd, struct hostent* hostEntry){
    
    int datafd = openData(connectfd, hostEntry);
    
    if (datafd == 0){
        return;
    }

    write(connectfd, "L\n", 2);     //send L command
    if(response(connectfd)){        //if command is accepted
        if(!fork()){                //fork
            dup2(datafd, 0);        //read into more
            
            if(execlp("more", "more", "-20", NULL) == -1){  //exec more
                perror("more failed");
            }
        }  
        else {
            wait(NULL);     //wait for child process to finish
        }
    }
    close(datafd);
}

void get(int connectfd, char* pathname, struct hostent* hostEntry){

    char path[MAXLINE];
    strcpy(path, pathname);
    char* prev;
    char* curr;

    curr = strtok(pathname, "/");       //seperate tokens in path name
    prev = curr;

    while((curr = strtok(NULL, "/")) != NULL){
        prev = curr; //prev is last part of pathname, used for name of file
    }

    int datafd = openData(connectfd, hostEntry);    //get data connection
    
    if(datafd == 0){            //data connection failed
        return;
    }
    
    char str[MAXLINE] = "G";
    strcat(str, path);
    strcat(str, "\n");              
    write(connectfd, str, strlen(str)); //build G<pathname>

    char buffer[MAXLINE];
    int i;

    if(response(connectfd)){        //successful response
        int fd = open(prev, O_WRONLY | O_CREAT, 0644);
        while((i = read(datafd, buffer, MAXLINE)) > 0){
            write(fd, buffer, i);       //read from datafd, write to file
        }
        close(fd);      //dont forget to close
    }
    close(datafd);      
}

void put(int connectfd, char* pathname, struct hostent* hostEntry){

    int datafd = openData(connectfd, hostEntry);
    
    if(datafd == 0){
        return;
    }
    
    char str[MAXLINE] = "P";
    strcat(str, pathname);
    strcat(str, "\n");

    if(access(pathname, R_OK) == -1){
        printf("Cannont access file %s\n", pathname);
        return;
    }

    write(connectfd, str, strlen(str)); //P<pathname>

    char buffer[MAXLINE];
    int i;

    if(response(connectfd)){


        int fd = open(pathname, O_RDONLY);
        if(fd == -1){
            perror("Open failed");  //cannont open file
            return;
        }
        while((i = read(fd, buffer, MAXLINE)) > 0){ //read from file, write to datafd
            write(datafd, buffer, i);
        }
        close(fd);
    }
    close(datafd);
}

void show(int connectfd, char* pathname, struct hostent* hostEntry){
    
    int datafd = openData(connectfd, hostEntry);
    
    if (datafd == 0){
        return;
    }

    char str[MAXLINE] = "G";
    strcat(str, pathname);
    strcat(str, "\n");
    write(connectfd, str, strlen(str)); //G<pathname>

    if(response(connectfd)){
        if(!fork()){
            dup2(datafd, 0);
            
            if(execlp("more", "more", "-20", NULL) == -1){  //read from datafd, pipe into more
                perror("more failed");
            }
        }  
        else {
            wait(NULL);     //wait for child to be finished
        }
    }
    close(datafd);
}
