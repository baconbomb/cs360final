/* Spencer Bacon
 * cs360
 * 4/20/18
 * mftp client
*/

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "mftp.h" 


int main(int argc, char **argv){
    struct hostent* hostEntry;
    struct sockaddr_in servAddr;
    int connectfd;

    if(argc != 2){
        fprintf(stderr, "incorrect usage\n %s <IPaddress | hostname>\n", argv[0]); 
        exit(EXIT_FAILURE);
    }

    connectfd = initSocket();

    if((hostEntry = gethostbyname(argv[1])) == NULL){
        herror("Incorrect hostname: ");
        exit(EXIT_FAILURE);
    }

    clientConnect(MY_PORT_NUMBER, connectfd, &servAddr, hostEntry); //connect to server
    
    fprintf(stdout, "Connected to server %s\n", hostEntry->h_name);

    startClient(connectfd, hostEntry);          //start client functions
}

